import random
class Guess:
    def __init__(self,name):
        self.name=name

    def guess(self,board):
        Guess=["Go back three spaces", "Advance to St.James Place", "Pay $50", "Collect 100$", "Go forward 2 spaces", "Advance to GO collect $200"]
        if board[self.position].name in 'Guess':
            print("Player has landed on Guess")
            x=random.choice(Guess)
            print(x)
            if x == "Go back three spaces":
                self.position -= 3
                print("You are now three spaces behind")
                self.evaluate_space()
            elif x == "Advance to St.James Place":
                self.position = 10
                print("You are now at St.James Place")
                self.evaluate_space()
            elif x == "Pay $50":
                self.money -= 50
                print("You have paid 50$")
            elif x == "Collect 100$":
                self.money += 100
                print("You got 100$")
            elif x == "Go forward 2 spaces":
                self.position += 2
                print("You are now two spaces forward")
                self.evaluate_space()
            elif x == "Advance to GO collect $200":
                self.position = 1
                self.money += 200
                print("You are now at Go and you collected 200$")
                self.evaluate_space(board)