import random
class Community:
    def __init__(self,name):
        self.name=name

    def community(self,board):
        if board[self.position].name in 'Community Chest':
            Community_Chest=["Go back three spaces", "Advance to Pacific Avenue", "Pay $200", "Collect 20$", "Go forward 5 spaces","Advance to GO collect $200"]
            print("Player has landed on Community Chest")
            CommunityChoice=random.choice(Community_Chest)
            print(CommunityChoice)
            if CommunityChoice == "Go back three spaces":
                self.position -= 3
                print("You are now three spaces behind")
                self.evaluate_space()
            elif CommunityChoice == "Advance to Pacific Avenue":
                self.position = 10
                print("You are now at Pacific Avenue")
                self.evaluate_space()
            elif CommunityChoice == "Pay $200":
                self.money -= 200
                print("You have paid 200$")
            elif CommunityChoice == "Collect 20$":
                self.money += 20
                print("You got 20$")
            elif CommunityChoice == "Go forward 5 spaces":
                self.position +=5
                print("You are now five spaces forward")
                self.evaluate_space()
            elif CommunityChoice == "Advance to GO collect $200":
                self.position = 1
                self.money += 200
                print("You are now at Go and you collected 200$")
                self.evaluate_space(board)