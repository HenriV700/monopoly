import random
from player import Player
from community import Community
from guess import Guess
from taxes import Taxes
# while number of players > 1
    # if player has a neighboor, player has option of buying houses
    # roll dice, sum of 2 random numbers
    # move spaces
    # if you land on property:
        # if the property has an owner:
            # pay rent
        # elif the property has no owner:
            #  if player has money:
                # you can buy
            # else if no money:
                # start auction
        # if you land on guess, pass for now
        # if you land on or pass go, collect money
class Property:

    owner = 'lester'

    def __init__(self,name, color,price,rent):
        self.name = str(name)
        self.color = color
        self.price = price
        self.rent = rent
class Go:
    def __init__(self,name):
        self.name=name

board = []

board.append(Property("Mediterranean Ave.",'purple',60,2))
board.append(Property("Baltic Ave.",'purple',60,4))
board.append(Property("Oriental Ave.",'LightBlue',100,6))
board.append(Property("Vermont Ave.",'LightBlue',100,7))
board.append(Property("Connecticut Ave.", 'LightBlue',120,8))
board.append(Property("St. Charles Place",'Pink',140,10))
board.append(Property("States Avenue",'Pink',140,11))
board.append(Property("Virginia_Avenue",'Pink',160,12))
board.append(Property("St.James Place",'Orange',180,13))
board.append(Property("Tennesse Avenue",'Orange',180,14))
board.append(Property("New York Avenue",'Orange',200,16))
board.append(Property("Kentucky Avenue",'Red',220,17))
board.append(Property("Indiana Avenue",'Red',220,18))
board.append(Property("Illinois Avenue",'Red',240,20))
board.append(Property("Atlantic Avenue",'Yellow',260,22))
board.append(Property("Ventnor Avenue",'Yellow',260,23))
board.append(Property("Marvin Gardens",'Yellow',280,24))
board.append(Property("Pacific Avenue",'Green',300,26))
board.append(Property("North Carolina Avenue",'Green',300,27))
board.append(Property("Pennsylvenia Avenue",'Green',320,28))
board.append(Property("Park Place",'DarkBlue',350,35))
board.append(Property("Board Walk",'DarkBlue',400,50))

board.insert(0, Go('GO'))
board.insert(15, Community('Community Chest'))
board.insert(21, Community('Community Chest'))
board.insert(29, Community('Community Chest'))
board.insert(19, Guess('Guess'))
board.insert(25, Guess('Guess'))
board.insert(34, Guess('Guess'))
board.insert(5, Taxes('Income Tax'))
board.insert(30, Taxes('Luxury Tax'))


p1 = Player()

for i in range(40):
    print(i+1)
    print(f'Cash start: {p1.money}, position: {p1.position}')
    p1.move(board)
    print(f'Cash end: {p1.money} \n')

"""

MAKE PLAYER ROLL DICE 4 TIMES.

AT EVERY ROLL, MOVE PLAYER TO A POSITION ON THE BOARD AND CHECK WHAT PROPERTY
EXISTS IN THAT POSITION.

THEN, CHARGE PLAYER 50% OF THE COST OF THE PROPERTY.

IF PLAYER PASSES GO, REWARD HIM WITH 200 MONEY.


"""

