import random
class Player:

    def __init__(self,):
        self.position = 0
        self.money = 2000
        self.properties=[]
        self.get_out_of_jail_card=[]


    @staticmethod
    def roll_dice():
        return sum([random.randrange(1, 7), random.randrange(1, 7)])

    def move(self, board):
        roll = self.roll_dice()
        future_position = self.position + roll

        print('rolled a', roll)

        if future_position == len(board):
            self.position = 0
            self.money += 200
            print('Passed GO, collected 200')

        elif future_position > len(board):
            diff = future_position - len(board)
            self.position = diff - 1
            self.money += 200
            print('Passed GO, collected 200')

        else:
            self.position = future_position

        self.evaluate_space(board)

    def evaluate_space(self,board):
        if board[self.position].name in ['GO','Water Works','Electricity Company']:
            print(f'Player has landed on {board[self.position].name}')
        elif board[self.position].name in ['Community Chest','Guess','Income Tax','Luxury Tax']:
            pass

        else:
            if board[self.position].owner is self or None:
                print(f'Player has landed on {board[self.position].name}')

            else:
                self.money -= board[self.position].price
                print(f'Player has landed on {board[self.position].name}')
                print(f'Rent has been deducted.')