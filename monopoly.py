import random
# while number of players > 1
    # if player has a neighboor, player has option of buying houses
    # roll dice, sum of 2 random numbers
    # move spaces
    # if you land on property:
        # if the property has an owner:
            # pay rent
        # elif the property has no owner:
            #  if player has money:
                # you can buy
            # else if no money:
                # start auction
        # if you land on guess, pass for now
        # if you land on or pass go, collect money

class Player:

    def __init__(self):
        self.position = 0
        self.money = 2000
        self.player1_property = []
        self.player2_property = []
        self.player1_balance = 2000
        self.player2_balance = 2000
        self.player1_position = 0
        self.player2_position = 0

    @staticmethod
    def roll_dice():
        return sum([random.randrange(1, 7), random.randrange(1, 7)])

    def move(self):
        roll = self.roll_dice()
        future_position = self.position + roll

        print('rolled a', roll)

        if future_position == len(board):
            self.position = 0
            self.money += 200
            print('Passed GO, collected 200')

        elif future_position > len(board):
            diff = future_position - len(board)
            self.position = diff - 1
            self.money += 200
            print('Passed GO, collected 200')

        else:
            self.position = future_position

        self.evaluate_space()

    def evaluate_space(self):
        if board[self.position].name in ['Community Chest', 'Guess', 'GO','Income Tax','Luxury Tax','Water Works','Electricity Company','Utilities','Railroad']:
            print(f'Player has landed on {board[self.position].name}')
            Guess=["Go back three spaces", "Advance to St.James Place", "Pay $50", "Collect 100$", "Go forward 2 spaces", "Advance to GO collect $200"]
            Community_Chest=["Go back three spaces", "Advance to Pacific Avenue", "Pay $200", "Collect 20$", "Go forward 5 spaces","Advance to GO collect $200"]

        elif board[self.position].name in 'Income Tax':
            print("Player has landed on Income Tax")
            self.money-=200
            print("Rent has been deducted.")

        elif board[self.position].name in 'Luxury Tax':
            print("Player has landed on Luxury Tax")
            self.money -= 75
            print("Rent has been deducted.")



        elif board[self.position].name in 'Guess':
            x=random.choice(Guess)
            print(x)
            if x == "Go back three spaces":
                self.position -= 3
                print("You are now three spaces behind")
                self.evaluate_space()
            elif x == "Advance to St.James Place":
                self.position = 10
                print("You are now at St.James Place")
                self.evaluate_space()
            elif x == "Pay $50":
                self.money -= 50
                print("You have paid 50$")
            elif x == "Collect 100$":
                self.money += 100
                print("You got 100$")
            elif x == "Go forward 2 spaces":
                self.position += 2
                print("You are now two spaces forward")
                self.evaluate_space()
            elif x == "Advance to GO collect $200":
                self.position = 1
                self.money += 200
                print("You are now at Go and you collected 200$")
                self.evaluate_space()

        elif board[self.position].name in 'Community Chest':
            CommunityChoice=random.choice(Community_Chest)
            print(CommunityChoice)
            if CommunityChoice == "Go back three spaces":
                self.position -= 3
                print("You are now three spaces behind")
                self.evaluate_space()
            elif CommunityChoice == "Advance to Pacific Avenue":
                self.position = 10
                print("You are now at Pacific Avenue")
                self.evaluate_space()
            elif CommunityChoice == "Pay $200":
                self.money -= 200
                print("You have paid 200$")
            elif CommunityChoice == "Collect 20$":
                self.money += 20
                print("You got 20$")
            elif CommunityChoice == "Go forward 5 spaces":
                self.position +=5
                print("You are now five spaces forward")
                self.evaluate_space()
            elif CommunityChoice == "Advance to GO collect $200":
                self.position = 1
                self.money += 200
                print("You are now at Go and you collected 200$")
                self.evaluate_space()

        elif board[self.position].name in 'Utilities':
            if board[self.position] not in self.player1_property and board[self.position] not in self.player2_property:
                answerP = input("Do you want to buy the property? (Yes/No)")
                if answerP == "Yes" or answerP == "yes":
                    self.money = self.player1.balance - 150
                    print("You have %s dollars remaining" % (self.player1.balance))
                elif answerP == "No"or answerP == "no":
                    print(" ")
                else:
                    print("Please answer Yes/No")
            elif board[self.player1.position] not in self.player1.property and board[self.player1.position] in self.player2.property:
                if self.player1.position[15] in self.player2.property:
                    print("You landed on another player's Utility! You have to pay them rent.")
                    self.player1.balance = self.player1balance - (self.roll_dice*10)
                    self.player2.balance = self.player2.balance + (self.roll_dice*10)
                    print("Your balance is now %s" % (self.player1.balance))
                elif self.player1.position[37] in self.player2.property:
                    print("You landed on another player's property! You have to pay them rent.")
                    self.player1.balance = self.player1.balance - (self.roll_dice*4)
                    self.player2.balance = self.player2.balance + (self.rolldice*4)
                    print("Your balance is now %s" % (self.player1.balance))
                else:
                    print("")
            else:
                print("You landed on your own property!")

        elif board[self.position].name in 'Railroads':
            if board[self.position] not in self.player2_property and board[self.position] not in self.player1_property:
                answerP = input("Do you want to buy the property? (Yes/No)")
                if answerP == "Yes" or answerP == "yes":
                    self.money = self.player2_balance - 200
                    print("You have %s dollars remaining" % (self.player2_balance))
                elif answerP == "No" or answerP == "no":
                    print(" ")
                else:
                    print("Please answer Yes/No")
            elif board[self.position] not in self.player2_property and board[self.position] in self.player1_property:
                print("You landed on another player's railroad! You have to pay them rent.")
                if self.player2.position[5] in self.player1_property and self.player2.position[12] in self.player1_property and self.player2.position[22] in self.player1_property and self.player2.position[31] in self.player1_property:
                    self.player2_balance = self.player2_balance - 200
                    self.player1_balance = self.player1_balance + 200
                    print("Your balance is now %s" % (self.player2_balance))
                elif self.palyer2.position[5] and self.player2.position[12] and self.player2.position[22] in self.player1_property or self.player2.position[5] and self.player2.position[22] and self.player2.position[31] in self.player1_property or self.player2.position[5] and self.player2.position[12] and self.player2.position[31] in self.player1_property or self.player2.position[12] and self.player2.position[22] and self.player2.position[31] in self.player1_property:
                    self.player2_balance = self.player2_balance - 100
                    self.player1_balance = self.player1_balance + 100
                    print("You balance is now %s" % (self.player2_balance))
                elif self.player2.position[5] and self.player2.position[12] or self.player2.position[5] and self.player2.position[22] or self.player2.position[5] and self.player2.position[31] or self.player2.position[12] and self.player2.position[22] or self.player2.position[12] and self.player2.position[31] or self.player2.position[22] and self.player2.position[31] in self.player1_property:
                    self.player2_balance = self.player2_balance - 50
                    self.player1_balance = self.player1_balance + 50
                    print("You balance is now %s" % (self.player2_balance))
                elif self.player2_position in self.player1_property:
                    self.player2_balance = self.player2_balance - 25
                    self.player1_balance = self.player1_balance + 25
                    print("You balance is now %s" % (self.player2_balance))
            else:
                print("You landed on your own property!")

        else:
            if board[self.position].owner is self or None:
                print(f'Player has landed on {board[self.position].name}')

            else:
                self.money -= board[self.position].price
                print(f'Player has landed on {board[self.position].name}')
                print(f'Rent has been deducted.')

class Property:

    owner = 'lester'

    def __init__(self,name, color,price,rent):
        self.name = str(name)
        self.color = color
        self.price = price
        self.rent = rent


class Special:
    def __init__(self, name):
        self.name = name

board = []

board.append(Property("Mediterranean Ave.",'purple',60,2))
board.append(Property("Baltic Ave.",'purple',60,4))
board.append(Property("Oriental Ave.",'LightBlue',100,6))
board.append(Property("Vermont Ave.",'LightBlue',100,7))
board.append(Property("Connecticut Ave.", 'LightBlue',120,8))
board.append(Property("St. Charles Place",'Pink',140,10))
board.append(Property("States Avenue",'Pink',140,11))
board.append(Property("Virginia_Avenue",'Pink',160,12))
board.append(Property("St.James Place",'Orange',180,13))
board.append(Property("Tennesse Avenue",'Orange',180,14))
board.append(Property("New York Avenue",'Orange',200,16))
board.append(Property("Kentucky Avenue",'Red',220,17))
board.append(Property("Indiana Avenue",'Red',220,18))
board.append(Property("Illinois Avenue",'Red',240,20))
board.append(Property("Atlantic Avenue",'Yellow',260,22))
board.append(Property("Ventnor Avenue",'Yellow',260,23))
board.append(Property("Marvin Gardens",'Yellow',280,24))
board.append(Property("Pacific Avenue",'Green',300,26))
board.append(Property("North Carolina Avenue",'Green',300,27))
board.append(Property("Pennsylvenia Avenue",'Green',320,28))
board.append(Property("Park Place",'DarkBlue',350,35))
board.append(Property("Board Walk",'DarkBlue',400,50))

board.insert(0, Special('GO'))
board.insert(36, Special('Community Chest'))
board.insert(21, Special('Community Chest'))
board.insert(29, Special('Community Chest'))
board.insert(19, Special('Guess'))
board.insert(25, Special('Guess'))
board.insert(34, Special('Guess'))
board.insert(5, Special('Income Tax'))
board.insert(30, Special('Luxury Tax'))
board.insert(5, Special('Railroads'))
board.insert(12, Special('Railroads'))
board.insert(22, Special('Railroads'))
board.insert(31, Special('Railroads'))
board.insert(15, Special('Utilities'))
board.insert(37, Special('Utilities'))

Railroads = ['Pennsylvania Railroad', 'Reading Railroad', 'Short Line', 'B.& O. Railroad']
Utilities = ['Water Works', 'Electrical Company']

player1 = Player()
player2 = Player()

for i in range(40):
    print(i+1)
    print(f'Cash start: {player1.money}, position: {player1.position}')
    player1.move()
    print(f'Cash end: {player1.money} \n')
    print(f'Cash start: {player2.money}, position: {player2.position}')
    player2.move()
    print(f'Cash end: {player2.money} \n')

"""

MAKE PLAYER ROLL DICE 4 TIMES.

AT EVERY ROLL, MOVE PLAYER TO A POSITION ON THE BOARD AND CHECK WHAT PROPERTY
EXISTS IN THAT POSITION.

THEN, CHARGE PLAYER 50% OF THE COST OF THE PROPERTY.

IF PLAYER PASSES GO, REWARD HIM WITH 200 MONEY.


"""
